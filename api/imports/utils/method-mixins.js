import _ from 'lodash'
import { Meteor } from 'meteor/meteor'

// Fun facts:
// - mixins defined first are applied first

/**
 * Copied from https://github.com/nabiltntn/loggedin-mixin/blob/master/loggedin-mixin.js
 * But adapted with default error.
 */
export function LoggedInMixin (methodOptions) {
  const errorDetails = _.defaultTo(methodOptions.notLoggedInError, {
    error: 'not-logged-in'
  })
  const runFunc = methodOptions.run
  methodOptions.run = function () {
    // this.connection is null when called from server
    if (this.connection && !this.userId) {
      const { error, message, reason } = errorDetails
      throw new Meteor.Error(error, message, reason) // TODO: refactor so that it's not dependant on the order -.-
    }
    // TODO: check roles
    return runFunc.call(this, ...arguments)
  }
  return methodOptions
}
