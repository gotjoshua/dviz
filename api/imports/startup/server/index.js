
import '../../api/server/expenses/publications'
import '../../api/server/groups/publications'
import '../../api/server/persons/publications'
import '../../api/server/pledges/publications'
import '../../api/server/publications'
import '../../api/server/users'
import './http'
