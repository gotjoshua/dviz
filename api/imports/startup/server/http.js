import _ from 'lodash'
import { Meteor } from 'meteor/meteor'
import { WebApp } from 'meteor/webapp'

Meteor.startup(function () {
  // seems like not enough as it doesn't add it for e.g. /sockjs
  // WebApp.connectHandlers.use(function (req, res, next) {
  //   res.setHeader('Access-Control-Allow-Origin', _.get(Meteor.settings, 'cors.origin', ''))
  //   return next()
  // })

  // Listen to incoming HTTP requests, can only be used on the server
  WebApp.rawConnectHandlers.use(function (req, res, next) {
    const origin = _.get(Meteor.settings, 'cors.origin')
    const headers = _.get(Meteor.settings, 'cors.headers')
    if (origin) res.setHeader('Access-Control-Allow-Origin', origin)
    if (headers) res.setHeader('Access-Control-Allow-Headers', headers)
    return next()
  })
})
