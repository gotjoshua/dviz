import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'
import { Persons } from '../persons/collection'
import { Groups } from './collection'

export const addGroup = new ValidatedMethod({
  name: 'groups.add',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    name: { type: String }
  }).validator(),
  run ({ name }) {
    console.log('Call to groups.add:', name)

    if (Groups.findOne({ name })) throw new Meteor.Error('already-exists', `group already exists: ${name}`)

    return Groups.insert({
      name,
      createdAt: new Date(),
      createdBy: Meteor.userId()
    })
  }
})

export const useInvite = new ValidatedMethod({
  name: 'groups.useInvite',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    personId: { type: String },
    groupId: { type: String }
  }).validator(),
  run ({ personId, groupId }) {
    console.log(`Call to groups.useInvite by ${this.userId}:`, arguments)
    if (!this.userId) throw new Meteor.Error('not-logged-in')

    const group = Groups.findOne({ _id: groupId })
    if (!group) throw new Meteor.Error('invalid-invite', 'Invalid invite code')
    const person = Persons.findOne({ _id: personId })
    if (!person) throw new Meteor.Error('invalid-person', 'Invalid Person')
    if (Meteor.user().person) throw new Meteor.Error('already-a-person', 'You are already connected to a person - contact admin')

    const affected = Meteor.users.update({ _id: this.userId }, { $addToSet: { persons: personId } })
    if (!affected) console.warn(`useInvite DB update (person=${personId}, groups=${groupId}) did't affect any data`)
  }
})
