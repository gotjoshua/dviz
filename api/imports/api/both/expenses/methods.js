import { CallPromiseMixin } from 'meteor/didericis:callpromise-mixin'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import Meteor from 'meteor/meteor'
import moment from 'moment'
import SimpleSchema from 'simpl-schema'
import { Expenses } from './collection'

export const addExpense = new ValidatedMethod({
  name: 'expenses.add',
  mixins: [CallPromiseMixin],
  validate: new SimpleSchema({
    group: { type: String },
    title: { type: String },
    payer: { type: String },
    date: { type: String },
    amount: { type: Number }
  }).validator(),
  run (entry) {
    console.log(`Call to expenses.add (sim=${this.isSimulation}):`, entry)
    // TODO: check group

    const dateMoment = moment(entry.date)
    if (!dateMoment.isValid()) throw new Meteor.Error('invalid-date', `Invalid moment string: ${entry.date}`, entry.date)
    entry.date = dateMoment.toDate()

    return Expenses.insert({
      ...entry,
      createdBy: Meteor.userId(),
      createdAt: new Date()
    })
  }
})

// Meteor.methods({
//   'expenses.insert' (entry) {
//     console.log('insert expense:', entry)
//     check(entry, Object)
//     Expenses.insert()
//   },
//   'expenses.update' (taskId, task) {
//     console.log('update expense:', taskId, task)
//     check(taskId, String)
//     check(task, String)
//     Expenses.update(taskId, {
//       $set: {
//         _id: taskId,
//         updatedAt: new Date(),
//         title: task
//       }
//     })
//   },
//   'expenses.remove' (taskId) {
//     check(taskId, String)
//     Expenses.remove(taskId)
//   }
// })
