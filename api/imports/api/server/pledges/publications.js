import { Meteor } from 'meteor/meteor'
import { Pledges } from '../../both/pledges/collection'

Meteor.publish('pledges', function () {
  if (!this.userId) return []
  return Pledges.find()
})
