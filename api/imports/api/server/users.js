import { Accounts } from 'meteor/accounts-base'
import SimpleSchema from 'simpl-schema'

// Ensuring every user has an email address, should be in server-side code
Accounts.validateNewUser((user) => {
  new SimpleSchema({
    _id: { type: String },
    emails: { type: Array, required: true, minCount: 1 },
    'emails.$': { type: Object },
    'emails.$.address': { type: String },
    'emails.$.verified': { type: Boolean },
    createdAt: { type: Date },
    services: { type: Object, blackbox: true },
    profile: { type: Object, blackbox: true } // TODO; restrict profile?
  }).validate(user)

  // Return true to allow user creation to proceed
  return true
})
