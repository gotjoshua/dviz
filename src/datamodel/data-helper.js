import { addPerson } from 'api/persons/methods'
import _ from 'lodash'
import moment from 'moment'

const baseProps = {
  sortable: true
}

export const FieldType = {
  TITLE: {
    ...baseProps,
    name: 'title',
    label: 'Title',
    field: 'title',
    format: t => t || '-',
    align: 'left',
    hints: ['title', 'what', 'reason']
  },
  PERSON: {
    ...baseProps,
    align: 'left',
    format: p => _.get(p, 'name', '-'),
    sort: (a, b, rowA, rowB) => a.name.localeCompare(b.name)
  },
  DATE: {
    ...baseProps,
    name: 'date',
    label: 'Date',
    field: 'date',
    align: 'left',
    format: d => moment(d).format('ll'),
    sort: (a, b, rowA, rowB) => moment(a).diff(moment(b)),
    hints: ['date', 'when']
  },
  AMOUNT: {
    ...baseProps,
    name: 'amount',
    label: 'Amount',
    field: 'amount',
    format: new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format,
    hints: ['amount', 'much']
  }
}

export async function findOrCreatePerson (name, persons) {
  const person = _.find(persons, { name })
  if (person) {
    return person
  } else {
    // TODO: use result of simulation
    return addPerson.callPromise({ name })
  }
}
