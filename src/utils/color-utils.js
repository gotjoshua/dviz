import ColorHash from 'color-hash'

export const colorHash = new ColorHash()

/**
   * isColorDark calculates the HSP value for a color, to determine its brightness.
   * A value of 127.5 and below means dark, and above 127.5 means bright
   * You can adjust this parameter to find really bright colors (maxHsp = 223)
   *
   * @param color
   * @param maxHsp - default 127.5 -
   * @returns {boolean|undefined}
   */
export function isColorDark (color, maxHsp = 127.5) {
  const rgb = rgbFromColor(color)
  if (!rgb) return undefined
  const { r, g, b } = rgb

  // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
  const hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b))

  // Using the HSP value, determine whether the color is light or dark
  return hsp <= maxHsp
};

/**
* returns either [r,g,b] from the color, or undefined if color is undefined
* @param color
*/
export function rgbFromColor (color) {
  if (!color) return undefined
  // Variables for red, green, blue values
  let r, g, b

  // Check the format of the color, HEX or RGB?
  if (color.match(/^rgb/)) {
    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/)

    r = color[1]
    g = color[2]
    b = color[3]
  } else {
    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +`0x${color.slice(1).replace(color.length < 5 && /./g, '$&$&')}`

    r = color >> 16
    g = (color >> 8) & 255
    b = color & 255
  }

  return { r, g, b }
};
