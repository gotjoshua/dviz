import _ from 'lodash'
import { Notify } from 'quasar'

export function hexEncodeStr (str) {
  var hex, i

  var result = ''
  for (i = 0; i < str.length; i++) {
    hex = str.charCodeAt(i).toString(16)
    result += ('000' + hex).slice(-4)
  }

  return result
}

export function errorNotify (err, msg = err.reason) {
  console.error(err)
  Notify.create({
    message: msg,
    color: 'negative'
  })
}

export function supplyGetters (supplyName, fieldNames) {
  const funcs = _.fromPairs(fieldNames.map(field => {
    if (field.startsWith('$')) return null
    return [field, function () {
      const result = this.$supply[supplyName][field]
      console.debug('supplyGetter call', field/* , 'on', this.$supply[supplyName] */, '->', result)
      return result
    }]
  }))
  console.debug('supplyGetter funcs:', funcs)
  return funcs
}

export function allSupplyGetters (Supply) {
  return supplyGetters(Supply.name, _.keys(Supply.meteor).filter(f => !f.startsWith('$')))
}
