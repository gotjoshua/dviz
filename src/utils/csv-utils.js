import _ from 'lodash'
import moment from 'moment'
import { Notify } from 'quasar'

export function toCsv (entities) {
  const fieldsOfEntites = entities.map(entity => {
    return _.toPairs(entity) // -> fields of entity: [ ['foo', 1], ['bar', 2] ]
  })
  const columns = _.chain(fieldsOfEntites)
    .flatMap(pairs => pairs.map(p => p[0])) // all existing fields
    .uniq()
    .value()
  const rows = [columns.map(col => [col, col]), ...fieldsOfEntites] // hacky way to prepend fields of entities list with a header
  const lines = rows.map(pairList => {
    const strFields = columns.map(field => { // start with fields as it defines the order
      const fieldForEntity = _.find(pairList, p => p[0] === field)
      let val = fieldForEntity ? fieldForEntity[1] : ''
      if (moment.isDate(val)) val = moment(val)
      if (moment.isMoment(val)) val = val.format()
      return val || ''
    })
    return strFields.join(';')
  })
  return lines.join('\n')
}

export async function csvExport (entities) {
  const csv = toCsv(entities)
  await navigator.clipboard.writeText(csv)
  Notify.create({
    message: 'CSV copied to clipboard',
    icon: 'file_copy'
  })
  // TODO: option to download as file
}
