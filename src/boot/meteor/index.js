import 'meteor.bundle' // Get meteor bundler in /node_modules
// Core
import { Meteor } from 'meteor/meteor'
import Vue from 'vue'
import VueMermaid from 'vue-mermaid'
// Packages
// import { Accounts } from 'meteor/accounts-base'
// import { Tracker } from 'meteor/tracker'
// Helper
import VueMeteorTracker from 'vue-meteor-tracker'
import VueSupply from 'vue-supply'

Vue.use(VueSupply)
Vue.use(VueMeteorTracker)

export default ({ Vue }) => {
  Vue.prototype.$meteor = Meteor
  // Vue.prototype.$meteor.tracker = Tracker
  // Vue.prototype.$meteor.accounts = Accounts
}

Vue.use(VueMermaid)
